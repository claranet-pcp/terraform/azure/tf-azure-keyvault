# **Azure Keyvault Module**

## **What is it?**

This module is to simplify the provisioning of Azure Keyvaults.

This module features the following:

```
* Keyvault creation
* Keyvault secret creation
```

# **Requirements**

Terraform 0.12 is the minimum requirements version for this module. This code has no external/other dependencies

# **Inputs**

# **Keyvault**

## **settings**

| Name                            | Description                                                                           |    Type     |    Default    | Required |
| ------------------------------- | ------------------------------------------------------------------------------------- | :---------: | :-----------: | :------: |
| resource_identifier             | identifier for Azure VM resource                                                      |   string    |      n/a      |  _yes_   |
| resource_group_name             | Specifies the name of the Resource Group in which the Key Vault should exist          |   string    |      n/a      |  _yes_   |
| location                        | Specifies the Azure Region where the Key Vault exists                                 |   string    | "northeurope" |    no    |
| sku                             | The name of the SKU used for the Key Vault. The options are: `standard`, `premium`.   |   string    |  "standard"   |    no    |
| enabled_for_deployment          | Allow Virtual Machines to retrieve certificates stored as secrets from the key vault. |    bool     |     false     |    no    |
| enabled_for_disk_encryption     | Allow Disk Encryption to retrieve secrets from the vault and unwrap keys.             |    bool     |     false     |    no    |
| enabled_for_template_deployment | Allow Resource Manager to retrieve secrets from the key vault.                        |    bool     |     false     |    no    |
| access_policies                 | List of access policies for the Key Vault.                                            |     any     |      []       |    no    |
| secrets                         | A map of secrets for the Key Vault.                                                   | map(string) |      {}       |    no    |
| secrets                         | A map of secrets for the Key Vault.                                                   | map(string) |      {}       |    no    |
| tags                            | A mapping of tags to assign to the resources.                                         |     map     |      {}       |    no    |

The `access_policies` object can have the following keys:

| Name                    | Type | Description                                                                                                                                                                                                                                         |
| ----------------------- | ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| group_names             | list | List of names of Azure AD groups.                                                                                                                                                                                                                   |
| object_ids              | list | List of object IDs of Azure AD users, security groups or service principals.                                                                                                                                                                        |
| user_principal_names    | list | List of user principal names of Azure AD users.                                                                                                                                                                                                     |
| certificate_permissions | list | List of certificate permissions. The options are: `backup`, `create`, `delete`, `deleteissuers`, `get`, `getissuers`, `import`, `list`, `listissuers`, `managecontacts`, `manageissuers`, `purge`, `recover`, `restore`, `setissuers` and `update`. |
| key_permissions         | list | List of key permissions. The options are: `backup`, `create`, `decrypt`, `delete`, `encrypt`, `get`, `import`, `list`, `purge`, `recover`, `restore`, `sign`, `unwrapkey`, `update`, `verify` and `wrapkey`.                                        |
| secret_permissions      | list | List of secret permissions. The options are: `backup`, `delete`, `get`, `list`, `purge`, `recover`, `restore` and `set`.                                                                                                                            |
| storage_permissions     | list | List of storage permissions. The options are: `backup`, `delete`, `deletesas`, `get`, `getsas`, `list`, `listsas`, `purge`, `recover`, `regeneratekey`, `restore`, `set`, `setsas` and `update`.                                                    |

# **Example**

```
#This example produces a single Azure Key Vault

module "key_vault" {
  source              = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-azure-keyvault?ref=v1.0.0"

  location             = "northeurope"
  resource_identifier = "somecomputername"
  resource_group_name  = module.resourcegroup.resource_group_name

  access_policies = [
    {
     user_principal_names = ["user@example.com"]
     secret_permissions   = ["get", "list"]
    },
    {
     group_names        = ["developers", "engineers"]
     secret_permissions = ["get", "list", "set"]
    },
  ]

  secrets = {
    "message" = "Hello, world!"
  }
}
```
