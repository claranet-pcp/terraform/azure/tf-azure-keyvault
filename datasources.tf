data "azuread_group" "main" {
  count         = length(local.group_names)
  display_name  = local.group_names[count.index]
}

data "azuread_user" "main" {
  count               = length(local.user_principal_names)
  user_principal_name = local.user_principal_names[count.index]
}


data "azurerm_client_config" "main" {}
