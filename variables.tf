
########## Keyvault  ##########

variable "module_depends_on" {
  type = any
  default = null
}

## settings

variable resource_identifier {
  description = "identifier for Azure VM resource"
  type        = string
}

variable resource_group_name {
  description = "Specifies the name of the Resource Group in which the Key Vault should exist"
  type        = string
}

variable location {
  description = "Specifies the Azure Region where the Key Vault exists"
  type        = string
  default     = "northeurope"
}

variable sku {
  type        = string
  description = "The name of the SKU used for the Key Vault. The options are: `standard`, `premium`."
  default     = "standard"
}

variable enabled_for_deployment {
  type        = bool
  description = "Allow Virtual Machines to retrieve certificates stored as secrets from the key vault."
  default     = false
}

variable enabled_for_disk_encryption {
  type        = bool
  description = "Allow Disk Encryption to retrieve secrets from the vault and unwrap keys."
  default     = false
}

variable enabled_for_template_deployment {
  type        = bool
  description = "Allow Resource Manager to retrieve secrets from the key vault."
  default     = false
}

variable access_policies {
  type        = any
  description = "List of access policies for the Key Vault."
  default     = []
}

variable secrets {
  type        = map(string)
  description = "A map of secrets for the Key Vault."
  default     = {}
}

variable tags {
  type        = map
  description = "A mapping of tags to assign to the resources."
  default     = {}
}
